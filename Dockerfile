FROM alpine:3.10 as base

RUN apk update && apk add --no-cache build-base python3-dev libxml2-dev libxslt-dev libressl-dev libffi-dev
RUN python3 -m pip install --user --no-warn-script-location Flask geoip2

FROM alpine:3.10

RUN apk update && apk add --no-cache python3

COPY --from=base /root/.local /root/.local
ENV PATH=/root/.local/bin:$PATH


WORKDIR /usr/src/app/
COPY GeoLite2-Country.mmdb /usr/share/GeoIP/GeoLite2-Country.mmdb
COPY src ./src

ENTRYPOINT ["python3"]
CMD ["/usr/src/app/src/main.py"]
